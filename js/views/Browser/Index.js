define(['marionette', 'templates', 'vent',
        'views/Browser/Controls', 'views/Browser/List'],
        function (Marionette, templates, vent, 
                  Controls, BrowserList) {
  'use strict';

  return Marionette.Layout.extend({
    template : templates.browser.index,

    regions: {
      header  : '#paginationControls',
      list    : '#list',
    },

    initialize : function(options) {
      this.collection.goTo(1);
      this.options.collection = this.collection;
    },

    onRender : function() {
      // this.header.show( new Controls(this.options) );
      this.list.show( new BrowserList(this.options) );

      // var self = this,
      // url = Utils.proxy('dataset/search/?defaultDS=t&reporters='+ app.state.reporters.join(',') +','+ app.state.entrez_id );
      // //-- Update the ds datasets based on our species
      // $.getJSON(url, function(d) {
      //   //-- now that we have the defaults, load up the view
      //   vent.trigger('NOTSUREYET', self.collection);
      //   self.collection.goTo(1);
      // });
    }

  });
});
