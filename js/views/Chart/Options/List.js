define(['marionette', 'templates', 'vent',
        'views/Chart/Options/ListItem'], 
    function (Marionette, templates, vent,
              ListItem) {
  'use strict';

  return Marionette.CollectionView.extend({
    itemView          : ListItem,

    onRender : function() {
      var self = this;

      this.$el.sortable({
        handle : '.btn-mini',
        placeholder: 'ui-state-highlight',
        update: function(event, ui) {
          var colors = self.model.get('DisplaySettings').get('colors');
          colors[self.options.factor] = $(this).sortable('toArray', {attribute: 'class'});
          self.model.get('DisplaySettings').set('colors', colors);
          self.model.get('DisplaySettings').trigger('change:colors')
        },
      });
      this.$el.disableSelection();
    },

  });
});
