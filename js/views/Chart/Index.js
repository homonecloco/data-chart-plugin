define(['marionette', 'templates', 'vent',
        'views/Chart/Views/Graph', 'views/Chart/Views/Image', 'views/Chart/Views/Export',
        'views/Chart/Header', 'views/Chart/Footer'],
        function (Marionette, templates, vent, 
                  Graph, Image, Export,
                  Header, Footer) {
  'use strict';

  return Marionette.Layout.extend({
    template : templates.chart.index,

    regions: {
      header   : '#header',
      content  : '#content',
      footer   : '#footer',
    },

    initialize : function() {
      //-- options.model and options.collection are passed in
      //-- model is the dataset/probe to display
      //-- collection is all the dataset/probe history

      var self = this;

      //-- If a new dataset/probe model is added to the collection, assume that
      //-- was just download and automatically display it (this means rerendering with
      //-- a new model)

      vent.on('chart:toggle', function(view) {
        switch(view) {
          case 'datachart':
            self.header.show( new Header(self.options) );
            self.content.show( new Graph(self.options) );
            break
          case 'image':
            self.header.close();
            self.content.show( new Image(self.options) );
            break;
          case 'export':
            self.header.close();
            self.content.show( new Export(self.options) );
            break;
        }
      });
    },

    //-- On new renders, always show the Graph by default
    onRender : function() {
      this.header.show( new Header(this.options) );
      this.content.show( new Graph(this.options) );
      this.footer.show( new Footer(this.options) );
    }

  });
});
